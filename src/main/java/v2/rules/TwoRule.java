package v2.rules;

import v2.SameDiceRule;

public class TwoRule extends SameDiceRule {

    public TwoRule() {
        super(2);
    }
}
