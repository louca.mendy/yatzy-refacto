package v2.rules;

import v2.SameDiceRule;

public class OneRule extends SameDiceRule {

    public OneRule() {
        super(1);
    }
}
