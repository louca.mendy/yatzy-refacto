package v2.rules;

import v2.SameDiceRule;

public class FiveRule extends SameDiceRule {

    public FiveRule() {
        super(5);
    }
}
