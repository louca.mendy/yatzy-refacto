package v2.rules;

import v2.SameDiceRule;

public class FourRule extends SameDiceRule {

    public FourRule() {
        super(4);
    }
}
