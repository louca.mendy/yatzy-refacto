package v2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

public class RolledDice {

    private final Integer d1;
    private final Integer d2;
    private final Integer d3;
    private final Integer d4;
    private final Integer d5;


    public RolledDice(Integer d1, Integer d2, Integer d3, Integer d4, Integer d5) {
        this.d1 = d1;
        this.d2 = d2;
        this.d3 = d3;
        this.d4 = d4;
        this.d5 = d5;
    }

    public List<Integer> getDiced() {
        return List.of(d1, d2,d3, d4, d5);
    }

    public static Map<Integer, Integer> toMap(List<Integer> dices) {
        Map<Integer, Integer> scores = new HashMap<>();
        IntStream.range(1, 7)
                        .forEach(i -> scores.put(i, 0));

        System.out.println(scores);

        dices.forEach(score -> {
            var value = scores.get(score);
            scores.put(score, value + 1);
        });
        return scores ;
    }
}
