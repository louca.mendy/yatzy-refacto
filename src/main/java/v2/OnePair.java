package v2;

import java.util.List;
import java.util.Map;

public class OnePair implements YatzyScore{

    @Override
    public Integer getScore(List<Integer> dices) {
        Map<Integer, Integer> mapScore = RolledDice.toMap(dices);

        return mapScore.entrySet()
                .stream()
                .filter(e -> e.getValue() == 2)
                .max(Map.Entry.comparingByKey())
                .stream()
                .mapToInt(es -> es.getKey() * 2)
                .sum();
    }
}
