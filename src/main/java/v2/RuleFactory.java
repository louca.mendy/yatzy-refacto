package v2;

import v2.rules.*;

import java.util.function.Supplier;

public enum RuleFactory {

    ONE_PAIR(OnePair::new),
    TWO_PAIR(Twopairs::new),
    THREE_OF_A_KIND(ThreeOfAKind::new),
    FOUR_OF_A_KIND(FourOfAKibd::new),
    SMALL_STRAIGHT(SmallStraight::new),
    LARGE_STRAIGHT(LargeStraight::new),
    FULL_HOUSE(FullHouse::new),
    ONES(OneRule::new),
    TWOS(TwoRule::new),
    THREES(ThreeRule::new),
    FOURS(FourRule::new),
    FIVES(FiveRule::new),
    SIXES(SixRule::new);

    private final Supplier<YatzyScore> constructor;

    RuleFactory(Supplier<YatzyScore> constructor) {
        this.constructor = constructor;
    }

    public YatzyScore create() {
        return constructor.get();
    }

}
