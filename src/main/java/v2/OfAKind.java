package v2;

import java.util.List;
import java.util.Map;

public abstract class OfAKind {

    private Integer occurence ;

    public OfAKind(Integer occurence) {
        this.occurence = occurence;
    }

    protected Integer ofAKibdScore(List<Integer> dices) {
       Map<Integer, Integer> scoreMap = RolledDice.toMap(dices);
        return scoreMap.entrySet()
                .stream()
                .filter(e -> e.getValue() >= occurence)
                .mapToInt(entry -> entry.getKey() * occurence)
                .sum();
    }

}
