package v2;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class Twopairs implements YatzyScore{

    @Override
    public Integer getScore(List<Integer> dices) {
        Map<Integer, Integer> mapScore = RolledDice.toMap(dices);

        List<Integer> pairs = mapScore.entrySet()
                .stream()
                .filter(e -> e.getValue() >= 2)
                .map(Map.Entry::getKey)
                .sorted(Comparator.reverseOrder())
                .limit(2)
                .toList();

        return pairs.size() == 2 ? pairs.stream().mapToInt(k -> k * 2).sum() : 0;
    }
}
