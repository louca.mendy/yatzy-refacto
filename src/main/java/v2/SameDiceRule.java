package v2;

import java.util.List;

public abstract class SameDiceRule implements YatzyScore, ScoreToHit<Integer> {

    private final Integer scoreToHit;

    public SameDiceRule(Integer scoreToHit) {
        this.scoreToHit = scoreToHit;
    }

    @Override
    public Integer getScore(List<Integer> dices) {
            return dices.stream()
                    .filter(scoreToHit::equals)
                    .mapToInt(Integer::intValue)
                    .sum();
    }
}
