package v2;

import java.util.List;

public class ThreeOfAKind extends OfAKind implements YatzyScore{
    private Integer occurence ;

    public ThreeOfAKind() {
        super(3);
    }

    @Override
    public Integer getScore(List<Integer> dices) {
       return ofAKibdScore(dices);
    }
}
