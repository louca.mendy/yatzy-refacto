package v2;

import java.util.List;

public class FourOfAKibd extends OfAKind implements YatzyScore{
    private Integer occurence ;

    public FourOfAKibd() {
        super(4);
    }

    @Override
    public Integer getScore(List<Integer> dices) {
       return ofAKibdScore(dices);
    }
}
