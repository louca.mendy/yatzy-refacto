package v2;


import java.util.*;

public class Yatzy {

    public Integer ones(List<Integer> dices) {
        return RuleFactory.ONES.create().getScore(dices);
    }

    public Integer twos(List<Integer> dices) {
       return RuleFactory.TWOS.create().getScore(dices);
    }

    public Integer threes(List<Integer> dices) {
        return RuleFactory.THREES.create().getScore(dices);
    }

    public Integer fours(List<Integer> dices) {
        return RuleFactory.FOURS.create().getScore(dices);
    }

    public Integer fives(List<Integer> dices) {
        return RuleFactory.FIVES.create().getScore(dices);
    }

    public Integer sixes(List<Integer> dices) {
        return RuleFactory.SIXES.create().getScore(dices);
    }

    public Integer onePair(List<Integer> dices) {
        return RuleFactory.ONE_PAIR.create().getScore(dices);
    }

    public Integer twoPair(List<Integer> dices) {
        return RuleFactory.TWO_PAIR.create().getScore(dices);
    }

    public Integer threeOfAKind(List<Integer> dices) {
        return RuleFactory.THREE_OF_A_KIND.create().getScore(dices);
    }

    public Integer fourOfAKind(List<Integer> dices) {
        return RuleFactory.FOUR_OF_A_KIND.create().getScore(dices);

    }

    public Integer smallStraight(List<Integer> dices) {
        return RuleFactory.SMALL_STRAIGHT.create().getScore(dices);
    }

    public Integer largeStraight(List<Integer> dices) {
        return RuleFactory.LARGE_STRAIGHT.create().getScore(dices);
    }

    public Integer fullHiuse(List<Integer> dices) {
        return RuleFactory.FULL_HOUSE.create().getScore(dices);
    }
}



