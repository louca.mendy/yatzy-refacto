package v2;

import com.sun.source.tree.Scope;

import java.util.List;

public class FullHouse implements YatzyScore {
    @Override
    public Integer getScore(List<Integer> dices) {
        int threesScore = exactlyThreesScore(dices);
        int twoScores = exactlyTwoScore(dices);

        if (twoScores > 0 && threesScore > 0) {
            return threesScore + twoScores ;
        }
        return 0 ;
    }

    private static int exactlyTwoScore(List<Integer> dices) {
        return RolledDice.toMap(dices)
                .entrySet()
                .stream()
                .filter(e -> e.getValue().equals(2))
                .mapToInt(e -> e.getKey() * 2)
                .sum();
    }

    private static int exactlyThreesScore(List<Integer> dices) {
        return RolledDice.toMap(dices)
                .entrySet()
                .stream()
                .filter(e -> e.getValue().equals(3))
                .mapToInt(e -> e.getKey() * 3)
                .sum();
    }
}
