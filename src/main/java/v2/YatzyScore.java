package v2;

import java.util.List;

public interface YatzyScore {

     Integer getScore(List<Integer> dices);
}
