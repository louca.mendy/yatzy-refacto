import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

public class Yatzy {

  protected int[] dice;

  public Yatzy(int d1, int d2, int d3, int d4, int _5) {
    dice = new int[5];
    dice[0] = d1;
    dice[1] = d2;
    dice[2] = d3;
    dice[3] = d4;
    dice[4] = _5;
  }

  public static int chance(int d1, int d2, int d3, int d4, int d5) {
    RolledDice rolledDice = new RolledDice(d1, d2, d3, d4, d5);

    return rolledDice
        .values()
        .stream()
        .reduce(0, Integer::sum);
  }

  public static int yatzy(int... dice) {
    RolledDice rolledDiceValues = new RolledDice(dice);
    final Optional<Integer> yatzy = rolledDiceValues.toMap()
        .values()
        .stream()
        .filter(v -> v == 5)
        .findFirst();

    return yatzy.isPresent() ? 50 : 0;
  }

  public static Integer number(int number, RolledDice rolledDice) {
    return rolledDice
        .toMap()
        .get(number) * number;
  }

  public static int ones(int d1, int d2, int d3, int d4, int d5) {
    RolledDice rolledDice = new RolledDice(d1, d2, d3, d4, d5);

    return number(1, rolledDice);
  }

  public static int twos(int d1, int d2, int d3, int d4, int d5) {
    RolledDice rolledDice = new RolledDice(d1, d2, d3, d4, d5);

    return number(2, rolledDice);
  }

  public static int threes(int d1, int d2, int d3, int d4, int d5) {
    RolledDice rolledDice = new RolledDice(d1, d2, d3, d4, d5);

    return number(3, rolledDice);
  }

  public static int score_pair(int d1, int d2, int d3, int d4, int d5) {
    RolledDice rolledDice = new RolledDice(d1, d2, d3, d4, d5);

    return numberOfAKind(2, rolledDice);
  }

  public static int two_pair(int d1, int d2, int d3, int d4, int d5) {
    RolledDice rolledDice = new RolledDice(d1, d2, d3, d4, d5);

    return rolledDice.toMap()
        .entrySet()
        .stream()
        .filter(k -> k.getValue() >= 2)
        .map(k -> k.getKey() * 2)
        .reduce(0, Integer::sum);
  }

  public static int four_of_a_kind(int d1, int d2, int d3, int d4, int d5) {
    RolledDice rolledDice = new RolledDice(d1, d2, d3, d4, d5);

    return numberOfAKind(4, rolledDice);
  }

  private static int numberOfAKind(Integer number, RolledDice rolledDice) {
    return rolledDice
        .toMap()
        .entrySet()
        .stream()
        .filter(k -> k.getValue() >= number)
        .map(Entry::getKey)
        .max(Comparator.naturalOrder())
        .map(i -> i * number)
        .orElse(0);
  }

  public static int three_of_a_kind(int d1, int d2, int d3, int d4, int d5) {
    RolledDice rolledDice = new RolledDice(d1, d2, d3, d4, d5);

    return numberOfAKind(3, rolledDice);
  }

  public static int smallStraight(int d1, int d2, int d3, int d4, int d5) {
    RolledDice rolledDice = new RolledDice(d1, d2, d3, d4, d5);
    final List<Integer> values = rolledDice.values().stream()
        .sorted(Comparator.naturalOrder())
        .collect(Collectors.toList());

    Integer[] diceTab = new Integer[values.size()];
    diceTab = values.toArray(diceTab);
    int straight = 0;
    for (int i = 0; i < diceTab.length - 1; i++) {
      if (diceTab[i + 1] - diceTab[i] == 1) {
        straight = straight + 1;
      } else {
        straight = 0;
      }
    }
    return straight >= 4 ? 15 : 0;
  }

  public static int largeStraight(int d1, int d2, int d3, int d4, int d5) {
    RolledDice rolledDice = new RolledDice(d1, d2, d3, d4, d5);
    final int count = (int) rolledDice.toMap().values().stream().filter(v -> v > 0).count();

    return count >= 5 ? 20 : 0;
  }

  public static int fullHouse(int d1, int d2, int d3, int d4, int d5) {
    RolledDice rolledDice = new RolledDice(d1, d2, d3, d4, d5);

    return rolledDice.toMap().entrySet().stream()
        .filter(k -> k.getValue() >= 2)
        .map(k -> k.getKey() * k.getValue())
        .reduce(0, Integer::sum);
  }

  public int fours() {
    RolledDice rolledDice = new RolledDice(this.dice);

    return number(4, rolledDice);
  }

  public int fives() {
    RolledDice rolledDice = new RolledDice(this.dice);

    return number(5, rolledDice);
  }

  public int sixes() {
    RolledDice rolledDice = new RolledDice(this.dice);

    return number(6, rolledDice);
  }
}



