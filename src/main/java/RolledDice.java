import java.util.List;
import java.util.Map;

public class RolledDice {

  private final int d1;
  private final int d2;
  private final int d3;
  private final int d4;
  private final int d5;

  public RolledDice(int d1, int d2, int d3, int d4, int d5) {
    this.d1 = d1;
    this.d2 = d2;
    this.d3 = d3;
    this.d4 = d4;
    this.d5 = d5;
  }

  public RolledDice(int[] dice) {
    this.d1 = dice[0];
    this.d2 = dice[1];
    this.d3 = dice[2];
    this.d4 = dice[3];
    this.d5 = dice[4];
  }

  public List<Integer> values() {
    return List.of(d1, d2, d3, d4, d5);
  }

  public Map<Integer, Integer> toMap() {
    Map<Integer, Integer> map = new java.util.HashMap<>(Map.of(1, 0, 2, 0, 3, 0, 4, 0, 5, 0, 6, 0));
    for (Integer n : values()) {
      map.put(n, map.get(n) + 1);
    }
    return map;
  }
}
