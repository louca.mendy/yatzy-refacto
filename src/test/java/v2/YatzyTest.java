package v2;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class YatzyTest {

/*  @Test
  public void chance_scores_sum_of_all_dice() {
    int expected = 15;
    int actual = Yatzy.chance(2, 3, 4, 5, 1);
    assertEquals(expected, actual);
    assertEquals(16, Yatzy.chance(3, 3, 4, 5, 1));
  }

  @Test
  public void yatzy_scores_50() {
    int expected = 50;
    int actual = Yatzy.yatzy(4, 4, 4, 4, 4);
    assertEquals(expected, actual);
    assertEquals(50, Yatzy.yatzy(6, 6, 6, 6, 6));
    assertEquals(0, Yatzy.yatzy(6, 6, 6, 6, 3));
  }*/

  @Test
  public void test_1s() {
    Yatzy yatzy = new Yatzy();
      assertEquals(1, (int) yatzy.ones(List.of(1, 2, 3, 4, 5)));
      assertEquals(2, (int) yatzy.ones(List.of(1, 2, 1, 4, 5)));
      assertEquals(0, (int) yatzy.ones(List.of(6, 2, 2, 4, 5)));
      assertEquals(4, (int) yatzy.ones(List.of(1, 2, 1, 1, 1)));
  }

  @Test
  public void test_2s() {
    Yatzy yatzy = new Yatzy();
    assertEquals(2, (int) yatzy.twos(List.of(1, 2, 3, 4, 5)));
    assertEquals(10, (int) yatzy.twos(List.of(2, 2, 2, 2, 2)));
  }

  @Test
  public void test_threes() {
    Yatzy yatzy = new Yatzy();
    assertEquals(6, (int) yatzy.threes(List.of(1, 2, 3, 2, 3)));
    assertEquals(12, (int) yatzy.threes(List.of(2, 3, 3, 3, 3)));
  }

  @Test
  public void fours_test() {
    Yatzy yatzy = new Yatzy();
    assertEquals(4, (int) yatzy.fours(List.of(1, 2, 3, 4, 5)));
    assertEquals(8, (int) yatzy.fours(List.of(4, 4, 5, 5, 5)));
    assertEquals(4, (int) yatzy.fours(List.of(4, 5, 5, 5, 5)));
  }

  @Test
  public void fives() {
    Yatzy yatzy = new Yatzy();
    assertEquals(10, (int) yatzy.fives(List.of(4, 4, 4, 5, 5)));
    assertEquals(15, (int) yatzy.fives(List.of(4, 4, 5, 5, 5)));
    assertEquals(20, (int) yatzy.fives(List.of(4, 5, 5, 5, 5)));
  }

  @Test
  public void sixes_test() {
    Yatzy yatzy = new Yatzy();
    assertEquals(0, (int) yatzy.sixes(List.of(3, 4, 4, 5, 5)));
    assertEquals(6, (int) yatzy.sixes(List.of(4, 4, 6, 5, 5)));
    assertEquals(18, (int) yatzy.sixes(List.of(6, 5, 6, 6, 5)));
  }

  @Test
  public void one_pair() {
    Yatzy yatzy = new Yatzy();
    assertEquals(6, (int) yatzy.onePair(List.of(3, 4, 3, 5, 6)));
    assertEquals(10, (int) yatzy.onePair(List.of(5, 3, 3, 3, 5)));
    assertEquals(12, (int) yatzy.onePair(List.of(5, 3, 6, 6, 5)));

  }

  @Test
  public void two_Pair() {
    Yatzy yatzy = new Yatzy();
    assertEquals(16, (int) yatzy.twoPair(List.of(3, 3, 5, 4, 5)));
    assertEquals(16, (int) yatzy.twoPair(List.of(3, 3, 5, 5, 5)));
  }

  @Test
  public void three_of_a_kind() {
    Yatzy yatzy = new Yatzy();
    assertEquals(9, (int) yatzy.threeOfAKind(List.of(3, 3, 3, 4, 5)));
    assertEquals(15, (int) yatzy.threeOfAKind(List.of(5, 3, 5, 4, 5)));
    assertEquals(9, (int) yatzy.threeOfAKind(List.of(3, 3, 3, 3, 5)));
  }


  @Test
  public void four_of_a_knd() {
    Yatzy yatzy = new Yatzy();
    assertEquals(12, (int) yatzy.fourOfAKind(List.of(3, 3, 3, 3, 5)));
    assertEquals(20, (int) yatzy.fourOfAKind(List.of(5, 5, 5, 4, 5)));
    assertEquals(12, (int) yatzy.fourOfAKind(List.of(3, 3, 3, 3, 3)));
  }


  @Test
  public void smallStraight() {
    Yatzy yatzy = new Yatzy();
    assertEquals(15, (int) yatzy.smallStraight(List.of(1, 2, 3, 4, 5)));
    assertEquals(15, (int) yatzy.smallStraight(List.of(2, 3, 4, 5,1 )));
    assertEquals(0, (int) yatzy.smallStraight(List.of(1, 2, 2, 4, 5)));

  }


  @Test
  public void largeStraight() {
    Yatzy yatzy = new Yatzy();
    assertEquals(20, (int) yatzy.largeStraight(List.of(6, 2, 3, 4, 5)));
    assertEquals(20, (int) yatzy.largeStraight(List.of(2, 3, 4, 5, 6)));
    assertEquals(0, (int) yatzy.largeStraight(List.of(1, 2, 2, 4, 5)));
  }

  @Test
  public void fullHouse() {
    Yatzy yatzy = new Yatzy();

    assertEquals(16, (int) yatzy.fullHiuse(List.of(2, 2, 4, 4, 4)));
    assertEquals(0, (int) yatzy.fullHiuse(List.of(2, 2, 6, 4, 4)));
  }
}
